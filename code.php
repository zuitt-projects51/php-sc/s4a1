<?php

class Building{

	protected $building;
	protected $floors;
	protected $address;

	public function __construct($building, $floors, $address){
		$this->building = $building;
		$this->floors = $floors;
		$this->address = $address;
	}

    public function getName(){
		return $this->building;
	}
   //setter
	public function setName($building){
		$this->building = $building;
	}

}   



class Condominium extends Building{
	//getter
	public function getName(){
		return $this->building;
	}

	//setter
	public function setName($building){
		$this->building = $building;
	}
};

$building = new Building("Caswynn Building", 8, "Quezon City, Philippines");
$condominium = new Condominium("Enzo Condo", 5, "Makati City, Philippines");
// $apartment = new Apartment("Enzo Condo", 5, "Makati City, Philippines");
